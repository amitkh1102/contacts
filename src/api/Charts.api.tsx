import axios from "axios";
import {
  CovidCasesPerCounrtyResponse,
  GlobalCovidCases,
  TimeLine,
} from "./Charts.api.types";

export const getTimelineData = async (): Promise<TimeLine> => {
  const response = await axios.get<TimeLine>(
    "https://disease.sh/v3/covid-19/historical/all?lastdays=all"
  );
  return response.data;
};

export const getDataPerCountry = async (): Promise<
  CovidCasesPerCounrtyResponse[]
> => {
  const response = await axios.get<CovidCasesPerCounrtyResponse[]>(
    "https://disease.sh/v3/covid-19/countries"
  );
  return response.data;
};

export const getGlobalData = async (): Promise<GlobalCovidCases> => {
  const response = await axios.get<GlobalCovidCases>(
    "https://disease.sh/v3/covid-19/all"
  );
  return response.data;
};
