import { useQuery } from "@tanstack/react-query";
import { getTimelineData } from "../../api/Charts.api";
import { useEffect } from "react";
import ReactApexChart from "react-apexcharts";
import { ApexOptions } from "apexcharts";

export const Graph = () => {
  const { data: cases } = useQuery(["timeline"], getTimelineData);
  let xAxis: string[] = [];
  let yAxis: ApexAxisChartSeries = [];
  let width = window.window.outerWidth;

  //   Create array if data available
  if (cases) {
    xAxis = Object.keys(cases.cases);
    for (const key in cases) {
      const data = {
        name: key,
        data: Object.values<number>(cases[key]).map((value) =>
          width < 500 ? Math.ceil(value / 100_000) : value
        ),
      };
      yAxis.push(data);
    }
  }

  //   Options
  const options: ApexOptions = {
    chart: {
      type: "line",
      dropShadow: {
        enabled: true,
        color: "#000",
        top: 18,
        left: 7,
        blur: 10,
      },
      toolbar: {
        show: false,
      },
    },
    title: {
      text: "Covid Cases",
      align: "left",
    },
    grid: {
      borderColor: "#e7e7e7",
      row: {
        colors: ["#f3f3f3", "transparent"],
        opacity: 0.5,
      },
    },
    xaxis: {
      type: "datetime",
      categories: xAxis,
      title: {
        text: "Timeline",
      },
    },
    yaxis: {
      title: {
        text: "Number of Cases" + (width < 500 ? " (per 100,000)" : ""),
      },
      min: 0,
      max: width < 500 ? 7200 : 720_000_000,
    },
    legend: {
      position: "top",
      horizontalAlign: "right",
      floating: true,
      offsetY: -25,
      offsetX: -10,
    },
  };

  return (
    <div id="chart">
      <ReactApexChart
        width="100%"
        series={yAxis}
        type="line"
        height={width < 500 ? 260 : 500}
        options={options}
      />
    </div>
  );
};
