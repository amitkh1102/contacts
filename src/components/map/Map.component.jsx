import { useQuery } from "@tanstack/react-query";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import { getDataPerCountry } from "../../api/Charts.api";
import { CovidCasesPerCounrtyResponse } from "../../api/Charts.api.types";

const position = [51.505, -0.09];

export const CovidMap = () => {
  const { data } = useQuery(["CasesPerCountry"], getDataPerCountry);
  return (
    <MapContainer
      center={[51.505, -0.09]}
      zoom={13}
      scrollWheelZoom={false}
      className="w-full h-52"
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {data?.map((covidCases, index) => (
        <Marker
          position={[covidCases.countryInfo.lat, covidCases.countryInfo.long]}
          key={index}
        >
          <Popup>
            country: {covidCases.country} cases: {covidCases.cases} deaths:{" "}
            {covidCases.deaths} recoverd: {covidCases.recovered}
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
};
