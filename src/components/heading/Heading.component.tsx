import { useLocation } from "react-router-dom";

export const Heading = () => {
  const location = useLocation();

  return (
    <h2 className="text-center p-4 bg-gradient-to-r from-blue-800 via-blue-500 to-blue-800 text-2xl font-bold text-white capitalize">
      {location.pathname.slice(1) === "contacts"
        ? "contacts page"
        : "charts and maps"}
    </h2>
  );
};
