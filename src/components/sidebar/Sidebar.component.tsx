import { useNavigate } from "react-router";

export const Sidebar = () => {
  const navigate = useNavigate();
  return (
    <div className="h-full w-24 flex flex-col border-2 border-black sticky top-0">
      <button
        className="py-8 text-lg px-2 underline "
        onClick={() => navigate("/contacts")}
      >
        Contact
      </button>
      <button
        className="py-8 text-lg px-2 underline border-t-2 border-b-2 border-black"
        onClick={() => navigate("/maps")}
      >
        Charts and Maps
      </button>
    </div>
  );
};
