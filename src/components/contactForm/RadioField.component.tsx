interface RadioProps {
  label: string;
  isActive?: boolean;
  onChange: (flag: boolean) => void;
}

export const RadioField = ({
  label,
  isActive = false,
  onChange,
}: RadioProps) => {
  return (
    <tr>
      <td>
        <label className="text-lg capitalize w-max whitespace-nowrap">
          {label}
        </label>
      </td>
      <td className="flex flex-col p-4">
        <fieldset>
          <div>
            <input
              type="radio"
              name="active"
              id="active"
              checked={isActive}
              onChange={() => onChange(true)}
            />
            <label className="text-lg capitalize md:ms-3" htmlFor="active">
              active
            </label>
          </div>
          <div>
            <input
              type="radio"
              name="inactive"
              id="inactive"
              checked={!isActive}
              onChange={() => onChange(false)}
            />
            <label className="text-lg capitalize md:ms-3" htmlFor="inactive">
              inactive
            </label>
          </div>
        </fieldset>
      </td>
    </tr>
  );
};
