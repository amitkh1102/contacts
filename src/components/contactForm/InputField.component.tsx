interface InputProps {
  label: string;
  id: string;
  value?: string;
  onChange: React.ChangeEventHandler<HTMLInputElement> | undefined;
}

export const InputField = ({ label, id, value, onChange }: InputProps) => {
  return (
    <tr>
      <td>
        <label
          htmlFor={id}
          className="text-lg capitalize w-max whitespace-nowrap"
        >
          {label}
        </label>
      </td>
      <td>
        <input
          type="text"
          name={id}
          id={id}
          className="md:mx-4 w-full"
          defaultValue={value}
          onChange={onChange}
        />
      </td>
    </tr>
  );
};
