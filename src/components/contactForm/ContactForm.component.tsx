import { MouseEventHandler, useState } from "react";
import { Contact, contactActions } from "../../store/reducers/contacts.reducer";
import { CustomButton } from "../customButton/CustomButton.component";
import { useDispatch, useSelector } from "react-redux";
import { InputField } from "./InputField.component";
import { RadioField } from "./RadioField.component";
import { eventActions } from "../../store/reducers/event.reducer";

interface ContactFormProps {
  id: number;
}

interface ContactFormSelectorState {
  contacts: Contact[];
}

export const ContactForm = ({ id }: ContactFormProps) => {
  const isEdit = id >= 0;

  // Hooks
  const contactsArray = useSelector<ContactFormSelectorState, Contact[]>(
    (state) => state.contacts
  );
  const [contact, setContact] = useState<Contact>(
    isEdit ? contactsArray[id] : {}
  );
  const dispatch = useDispatch();

  // Helper Functions
  const changeFirstName = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setContact({ ...contact, firstName: ev.target.value });
  };
  const changeLastName = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setContact({ ...contact, lastName: ev.target.value });
  };
  const setIsActive = (isActive: boolean) => {
    setContact({ ...contact, isActive });
  };
  const onFormSubmit: MouseEventHandler<HTMLButtonElement> | undefined = (
    ev
  ) => {
    dispatch({
      type: isEdit ? contactActions.UPDATE : contactActions.ADD_NEW,
      payload: { contact, id },
    });
    dispatch({
      type: eventActions.CONTACT_CLOSE,
    });
  };

  // Returnable
  return (
    <form className="flex flex-col w-full md:w-max items-center my-8 gap-4 md:p-8">
      {/* form heading */}
      <h2 className="text-xl capitalize text-center font-bold">
        {isEdit ? "edit" : "create"} contact screen
      </h2>
      <div className="border-spacing-4  border-4 border-black p-8">
        {/* form */}
        <table>
          <tbody>
            <InputField
              id="firstName"
              label="first name"
              value={contact.firstName}
              onChange={changeFirstName}
            />
            <InputField
              id="lastName"
              label="last name"
              value={contact.lastName}
              onChange={changeLastName}
            />
            <RadioField
              isActive={contact.isActive}
              label="status :"
              onChange={setIsActive}
            />
          </tbody>
        </table>
      </div>

      {/* Submit button */}
      <CustomButton type="submit" onClick={onFormSubmit}>
        save contact
      </CustomButton>
    </form>
  );
};
