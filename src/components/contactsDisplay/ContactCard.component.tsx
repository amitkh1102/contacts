import { useDispatch } from "react-redux";
import { Contact, contactActions } from "../../store/reducers/contacts.reducer";
import { eventActions } from "../../store/reducers/event.reducer";

interface ContactCardProps {
  contact: Contact;
  index: number;
}

export const ContactCard = ({ contact, index }: ContactCardProps) => {
  const dispatch = useDispatch();

  //   Helper functions
  const onEdit = () =>
    dispatch({ type: eventActions.EDIT_CONTACT, payload: { index } });
  const onDelete = () =>
    dispatch({ type: contactActions.DELETE, payload: { id: index } });

  return (
    <div className="flex flex-col items-center w-52 sm:w-64 h-64 gap-4 border-black border-2 p-4 rounded">
      <h1 className="text-ellipsis overflow-hidden text-center">
        {contact.firstName} {contact.lastName}
      </h1>
      <div className="flex items-center gap-4">
        <div
          className={
            "w-4 h-4 rounded-full " +
            (contact.isActive ? "bg-green-500" : "bg-black")
          }
        ></div>
        <p>{contact.isActive ? "active" : "in-active"}</p>
      </div>
      <button
        className="capitalize p-4 bg-gray-300 w-full border-2 border-black rounded-lg font-bold"
        onClick={onEdit}
      >
        Edit
      </button>
      <button
        className="capitalize p-4 bg-gray-300 w-full border-2 border-black rounded-lg font-bold"
        onClick={onDelete}
      >
        Delete
      </button>
    </div>
  );
};
