import { useSelector } from "react-redux";
import { Contact } from "../../store/reducers/contacts.reducer";
import { ContactCard } from "./ContactCard.component";
import { ReactComponent as Cross } from "../../assets/no_contact_found.svg";

export const ContactsDisplay = () => {
  const contacts = useSelector<{ contacts: Contact[] }, Contact[]>(
    (state) => state.contacts
  );

  return (
    <div className="flex flex-wrap justify-evenly gap-4">
      {contacts.length === 0 && (
        <div className="flex gap-4 items-center justify-evenly mt-10 w-full sm:w-72 p-2 border-2 border-black rounded">
          <Cross className="w-1/2" />
          <h1 className="">
            no contact found. please add a contact fromt the create contact
            button above.
          </h1>
        </div>
      )}
      {contacts.map((contact, index) => (
        <ContactCard contact={contact} index={index} key={index} />
      ))}
    </div>
  );
};
