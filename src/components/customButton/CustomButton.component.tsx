import { MouseEventHandler } from "react";

export const CustomButton = ({
  children,
  type = "button",
  onClick,
}: {
  children: any;
  type?: "button" | "submit" | "reset" | undefined;
  onClick: MouseEventHandler<HTMLButtonElement> | undefined;
}) => {
  return (
    <button
      className="capitalize p-4 bg-gray-300 w-max border-4 border-black rounded-lg font-bold"
      onClick={onClick}
      type={type}
    >
      {children}
    </button>
  );
};
