import { combineReducers } from "redux";
import { contactReducer } from "./reducers/contacts.reducer";
import { eventReducer } from "./reducers/event.reducer";

export const rootReducer = combineReducers({
  contacts: contactReducer,
  events: eventReducer,
});
