import { Reducer } from "redux";

export interface Contact {
  firstName?: string;
  lastName?: string;
  isActive?: boolean;
}

export const contactActions = {
  ADD_NEW: "CONTACT/ADD_NEW",
  DELETE: "CONTACT/DELETE",
  UPDATE: "CONTACT/UPDATE",
};

const INITIAL_STATE: Contact[] = [];

export const contactReducer: Reducer = (
  state: Contact[] = INITIAL_STATE,
  { type, payload }
) => {
  switch (type) {
    case contactActions.ADD_NEW:
      return [...state, payload.contact];

    case contactActions.DELETE:
      return state.filter((_, index) => index != payload.id);

    case contactActions.UPDATE:
      return state.map((elem, index) =>
        index != payload.id ? elem : payload.contact
      );

    default:
      return state;
  }
};
