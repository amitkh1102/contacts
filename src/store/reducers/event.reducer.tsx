import { AnyAction, Reducer } from "redux";

export interface EVENT_PAYLOAD {
  editIndex: number;
  createNew?: boolean;
}

export const eventActions = {
  CREATE_NEW_CONTACT: "EVENT/ENABLE_CREATE_NEW_CONTACT_FORM",
  EDIT_CONTACT: "EVENT/ENABLE_EDIT_CONTACT_FORM",
  CONTACT_CLOSE: "EVENT/DISABLE_CREATE_NEW_CONTACT_FORM",
};

const INTIAL_STATE: EVENT_PAYLOAD = {
  editIndex: -1,
  createNew: false,
};

export const eventReducer: Reducer = (
  state: EVENT_PAYLOAD = INTIAL_STATE,
  { type, payload }: AnyAction
): EVENT_PAYLOAD => {
  switch (type) {
    case eventActions.CREATE_NEW_CONTACT:
      return { ...state, createNew: true };

    case eventActions.CONTACT_CLOSE:
      return { editIndex: -1, createNew: false };

    case eventActions.EDIT_CONTACT:
      return { ...state, editIndex: payload.index };

    default:
      return state;
  }
};
