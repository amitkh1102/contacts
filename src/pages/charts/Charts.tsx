import { useQuery } from "@tanstack/react-query";
import { Graph } from "../../components/charts/graph.component";
import { CovidMap } from "../../components/map/Map.component.jsx";
import { getGlobalData } from "../../api/Charts.api";

export const Charts = () => {
  const { data } = useQuery(["WorldCases"], getGlobalData);
  return (
    <div className="w-full md:p-4 bg-zinc-200">
      <Graph />
      <p>
        <span className="font-bold">active cases:</span> {data?.active} |{" "}
        <span className="font-bold">total cases:</span> {data?.cases} |{" "}
        <span className="font-bold">total deaths:</span> {data?.deaths} |{" "}
        <span className="font-bold">total recovered:</span> {data?.recovered}
      </p>
      <h2 className="font-bold underline text-lg">map per counrty:</h2>
      <CovidMap />
    </div>
  );
};
