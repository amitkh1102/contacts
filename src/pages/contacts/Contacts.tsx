import { CustomButton } from "../../components/customButton/CustomButton.component";
import { ContactForm } from "../../components/contactForm/ContactForm.component";
import { ContactsDisplay } from "../../components/contactsDisplay/ContactsDisplay.component";
import { useDispatch, useSelector } from "react-redux";
import {
  EVENT_PAYLOAD,
  eventActions,
} from "../../store/reducers/event.reducer";

export const Contacts = () => {
  // Hooks
  const events = useSelector<{ events: EVENT_PAYLOAD }, EVENT_PAYLOAD>(
    (state) => state.events
  );
  const dispatch = useDispatch();
  const isFormActice = events.createNew || events.editIndex >= 0;

  return (
    <div className="w-full bg-zinc-200 flex flex-col items-center gap-4 md:p-4 pt-4">
      {!isFormActice && (
        <>
          <CustomButton
            onClick={() => dispatch({ type: eventActions.CREATE_NEW_CONTACT })}
          >
            create new contact
          </CustomButton>
          <ContactsDisplay />
        </>
      )}
      {isFormActice && <ContactForm id={events.editIndex} />}
    </div>
  );
};
