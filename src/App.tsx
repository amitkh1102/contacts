import "./App.css";
import { Navigate, Route, Routes } from "react-router";
import { Contacts } from "./pages/contacts/Contacts";
import { Charts } from "./pages/charts/Charts";
import { Heading } from "./components/heading/Heading.component";
import { Sidebar } from "./components/sidebar/Sidebar.component";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="flex flex-col min-h-screen border-4 border-blue-800">
        <Heading />
        <div className="grow h-full flex">
          <Sidebar />
          <Routes>
            <Route path="/" element={<Navigate to={"/contacts"} replace />} />
            <Route path="/contacts" Component={Contacts} />
            <Route path="/maps" Component={Charts} />
          </Routes>
        </div>
      </div>
    </QueryClientProvider>
  );
}

export default App;
